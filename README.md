# 北海道旅行企划

TODOs
- 吃
- 各种公交
- 买
- 酒店接送

## 日程

- 2/8 从上海出发到札幌
- 2/9 小樽
- 2/10 札幌滑雪
- 2/11 网走
- 2/12 网走
- 2/13 从札幌出发到上海

## 2/8

### 转移方程

上海浦东机场到北广岛Classe酒店
1. 上海浦东机场到东京羽田机场
1. 东京羽田机场到札幌新千岁机场

    [东京羽田机场国际航站楼到D2航站楼](https://www.google.com/maps/dir/Haneda+Airport+International+Terminal,+2+Chome-6+Hanedakuko,+%C5%8Cta,+Tokyo,+Japan/Haneda-Airport+Terminal+2+Station,+3+Chome-4-2+Hanedakuko,+%C5%8Cta,+Tokyo+144-8525,+Japan/@35.5453939,139.7686864,15z/data=!4m14!4m13!1m5!1m1!1s0x601861698f59ec7f:0x976220b9b50b53f3!2m2!1d139.7690427!2d35.5450019!1m5!1m1!1s0x601863f69cf14f7b:0x4ec695ea34e14d41!2m2!1d139.7879961!2d35.5507002!3e3)

1. [新千岁机场到札幌Classe酒店](https://www.google.com/maps/dir/New+Chitose+Airport,+Bibi,+Chitose,+Hokkaido,+Japan/Sapporo+Classe+Hotel,+7+Chome-1-2+Minami+1+J%C5%8Dnishi,+Ch%C5%AB%C5%8D-ku,+Sapporo-shi,+Hokkaid%C5%8D+060-0061,+Japan/@42.9263835,141.3744259,11z/data=!3m1!4b1!4m18!4m17!1m5!1m1!1s0x5f7520459d5c1add:0x1aba607ce5f681f6!2m2!1d141.6704786!2d42.7925942!1m5!1m1!1s0x5f0b299ad8ce341d:0x6caed3fbac80c426!2m2!1d141.3476159!2d43.0579536!2m3!6e0!7e2!8j1549614600!3e3)

[札幌Classe酒店到北海道厅](https://www.google.com/maps/dir/Sapporo+Classe+Hotel,+7+Chome-1-2+Minami+1+J%C5%8Dnishi,+Ch%C5%AB%C5%8D-ku,+Sapporo-shi,+Hokkaid%C5%8D+060-0061,+Japan/Hokkaid%C5%8D+Government+Office/@43.0617424,141.3473994,15.75z/data=!4m18!4m17!1m5!1m1!1s0x5f0b299ad8ce341d:0x6caed3fbac80c426!2m2!1d141.3476159!2d43.0579536!1m5!1m1!1s0x5f0b299f007507ad:0xbb215d6e0de69db7!2m2!1d141.3468325!2d43.0643092!2m3!6e0!7e2!8j1549659600!3e3) via 走走, ~10 min

[北海道厅到啤酒博物馆](https://www.google.com/maps/dir/Hokkaid%C5%8D+Government+Office,+6+Chome+Kita+3+Jonishi,+Chuo+Ward,+Sapporo,+Hokkaido,+Japan/Sapporo+Beer+Museum,+9+Chome-1-%EF%BC%91+Kita+7+Johigashi,+Higashi+Ward,+Sapporo,+Hokkaido,+Japan/@43.0712837,141.3488137,15z/data=!3m1!4b1!4m19!4m18!1m5!1m1!1s0x5f0b299f007507ad:0xbb215d6e0de69db7!2m2!1d141.3468325!2d43.0643092!1m5!1m1!1s0x5f0b29842f2cf6d1:0x1360c6faefc5e7cb!2m2!1d141.3689124!2d43.0714671!2m3!6e0!7e2!8j1547020800!3e3!5i3) via 走走, ~30 min

*啤酒博物馆到火车博物馆？*

[啤酒博物馆到札幌电视塔](https://www.google.com/maps/dir/Sapporo+Beer+Museum,+9+Chome-1-%EF%BC%91+Kita+7+Johigashi,+Higashi+Ward,+Sapporo,+Hokkaido,+Japan/SAPPORO+TV+Tower,+1+Chome+Odorinishi,+Chuo+Ward,+Sapporo,+Hokkaido,+Japan/@43.0694036,141.3515631,15z/data=!3m2!4b1!5s0x5f0b299d508fc785:0xa89c33d35137c190!4m18!4m17!1m5!1m1!1s0x5f0b29842f2cf6d1:0x1360c6faefc5e7cb!2m2!1d141.3689124!2d43.0714671!1m5!1m1!1s0x5f0b299d5f87648d:0xe2041a78c3222031!2m2!1d141.3564323!2d43.061105!2m3!6e0!7e2!8j1547020800!3e3) via 走走 xor 公交, ~30 min

[札幌电视塔到白色恋人公园](https://www.google.com/maps/dir/SAPPORO+TV+Tower,+1+Chome+Odorinishi,+Chuo+Ward,+Sapporo,+Hokkaido,+Japan/Shiroi+Koibito+Park/@43.0730242,141.2964744,14z/data=!3m2!4b1!5s0x5f0b299d508fc785:0xa89c33d35137c190!4m18!4m17!1m5!1m1!1s0x5f0b299d5f87648d:0xe2041a78c3222031!2m2!1d141.3564323!2d43.061105!1m5!1m1!1s0x5f0b281264c7cb53:0xc21e3cb8d40a0191!2m2!1d141.2715986!2d43.0886783!2m3!6e0!7e2!8j1547035200!3e3) via 地铁, ~30 min

[白色恋人公园到北海道神宫](https://www.google.com/maps/dir/Shiroi+Koibito+Park/%E8%9D%A6%E5%A4%B7%E5%9C%8B%E4%B8%80%E4%B9%8B%E5%AE%AE+%E5%8C%97%E6%B5%B7%E9%81%93%E7%A5%9E%E5%AE%AE/@43.0693443,141.2881221,14z/data=!3m1!5s0x5f0b299d508fc785:0xa89c33d35137c190!4m19!4m18!1m5!1m1!1s0x5f0b281264c7cb53:0xc21e3cb8d40a0191!2m2!1d141.2715986!2d43.0886783!1m5!1m1!1s0x5f0b299db24ab3a1:0xc2d1cbb1524ceaa6!2m2!1d141.3077595!2d43.0543456!2m3!6e0!7e2!8j1547035200!3e3!5i1) via 地铁 xor 公交, ~30 min

[北海道神宫到札幌Classe酒店拿行李](https://www.google.com/maps/dir/%E8%9D%A6%E5%A4%B7%E5%9C%8B%E4%B8%80%E4%B9%8B%E5%AE%AE+%E5%8C%97%E6%B5%B7%E9%81%93%E7%A5%9E%E5%AE%AE/Sapporo+view+hotel,+8+Chome+Odorinishi,+Chuo+Ward,+Sapporo,+Hokkaido,+Japan/@43.0578839,141.3182035,15z/data=!3m2!4b1!5s0x5f0b299d508fc785:0xa89c33d35137c190!4m18!4m17!1m5!1m1!1s0x5f0b299db24ab3a1:0xc2d1cbb1524ceaa6!2m2!1d141.3077595!2d43.0543456!1m5!1m1!1s0x5f0b299a599e75ed:0xc98051102e770376!2m2!1d141.3446963!2d43.0588028!2m3!6e0!7e2!8j1547035200!3e3) via 地铁 xor 公交, ~20 min

[札幌Classe酒店到定山溪酒店](https://www.google.com/maps/dir/Sapporo+view+hotel,+8+Chome+Odorinishi,+Chuo+Ward,+Sapporo,+Hokkaido,+Japan/%E5%AE%9A%E5%B1%B1%E6%B8%93%E3%83%9B%E3%83%86%E3%83%AB/@43.0057331,141.2226099,12z/data=!3m1!5s0x5f0b299d508fc785:0xa89c33d35137c190!4m18!4m17!1m5!1m1!1s0x5f0b299a599e75ed:0xc98051102e770376!2m2!1d141.3446963!2d43.0588028!1m5!1m1!1s0x0:0x1a5249e226336383!2m2!1d141.161558!2d42.965078!2m3!6e0!7e2!8j1547035200!3e3) via 公交, ~1 hr

> 是否可以叫定山溪酒店派车来札幌接我们？

> 是否可以发邮件叫[札幌Classe酒店]来机场接我们？

### 住

[札幌Classe酒店][札幌Classe酒店] aka. Sapporo Classe Hotel

### 打卡

- [北海道厅](https://www.google.com/maps/place/Hokkaid%C5%8D+Government+Office/@43.0739885,141.3128527,13.25z/data=!4m5!3m4!1s0x5f0b299f007507ad:0xbb215d6e0de69db7!8m2!3d43.0643103!4d141.3468361) aka. Hokkaidou Government Office
- [白色恋人公园](https://www.google.com/maps/place/Shiroi+Koibito+Park/@43.0739885,141.3128527,13z/data=!4m12!1m6!3m5!1s0x5f0b299f007507ad:0xbb215d6e0de69db7!2sHokkaid%C5%8D+Government+Office!8m2!3d43.0643103!4d141.3468361!3m4!1s0x0:0xc21e3cb8d40a0191!8m2!3d43.088675!4d141.2715948) aka. Shiroi Koibito Park
- [札幌电视塔](https://www.google.com/maps/place/SAPPORO+TV+Tower/@43.0739885,141.3128527,13z/data=!3m1!5s0x5f0b299d508fc785:0xa89c33d35137c190!4m12!1m6!3m5!1s0x5f0b299f007507ad:0xbb215d6e0de69db7!2sHokkaid%C5%8D+Government+Office!8m2!3d43.0643103!4d141.3468361!3m4!1s0x0:0xe2041a78c3222031!8m2!3d43.0611044!4d141.3564277) aka. Sapporo TV Tower
- [札幌啤酒博物馆](https://www.google.com/maps/place/Sapporo+Beer+Museum/@43.0739885,141.3128527,13z/data=!4m5!3m4!1s0x0:0x1360c6faefc5e7cb!8m2!3d43.0714663!4d141.368916) aka. Sapporo Beer Museum
- [北海道神宫](https://www.google.com/maps/place/Hokkaido+Shrine+Honden/@43.0667014,141.3084893,13z/data=!4m5!3m4!1s0x5f0b29dd1e02635d:0xd44a8b058ac7c482!8m2!3d43.0540645!4d141.3075615) aka. Hokkaidou Shrine
- *火车博物馆?*

## 2/9

### 转移方程

定山溪酒店到小樽站
1. [定山溪酒店到札幌站](https://www.google.com/maps/dir/Japan,+%E3%80%92061-2303+Hokkaid%C5%8D,+Sapporo-shi,+Minami-ku,+J%C5%8Dzankeionsennishi,+4+Chome%E2%88%92340,+%E5%AE%9A%E5%B1%B1%E6%B8%93%E3%83%9B%E3%83%86%E3%83%AB/Sapporo+Station,+4+Chome+Kita+6+Jonishi,+Kita,+Sapporo,+Hokkaido,+Japan/@43.0160726,141.1967934,12z/data=!3m1!4b1!4m18!4m17!1m5!1m1!1s0x5f0ad21891910975:0x1a5249e226336383!2m2!1d141.161558!2d42.965078!1m5!1m1!1s0x5f0b2974d2c3f903:0xa5e2b18cdd4a47a5!2m2!1d141.3507553!2d43.0686606!2m3!6e0!7e2!8j1552204800!3e3) via 公交
1. [札幌站到小樽站](https://www.google.com/maps/dir/Sapporo+Station,+4+Chome+Kita+6+Jonishi,+Kita,+Sapporo,+Hokkaido,+Japan/Otaru+Station,+2+Chome-22+Inaho,+Otaru,+Hokkaido,+Japan/@43.1283999,141.033272,11z/am=t/data=!4m18!4m17!1m5!1m1!1s0x5f0b2974d2c3f903:0xa5e2b18cdd4a47a5!2m2!1d141.3507553!2d43.0686606!1m5!1m1!1s0x5f0ae1acc9f79d0f:0x459dd44dd4dcb36b!2m2!1d140.9937088!2d43.1977462!2m3!6e0!7e2!8j1549785600!3e3) via JR, ~50 min

[小樽站到小樽运河](https://www.google.com/maps/dir/Otaru+Station,+2+Chome-22+Inaho,+Otaru,+Hokkaido,+Japan/Otaru+Canal/@43.1984571,140.9980682,16z/data=!3m1!5s0x5f0b2974d2fff7a5:0x4ceb5b0533433580!4m18!4m17!1m5!1m1!1s0x5f0ae1acc9f79d0f:0x459dd44dd4dcb36b!2m2!1d140.9937088!2d43.1977462!1m5!1m1!1s0x5f0ae04d246170e1:0xcde965c5a0dafcd4!2m2!1d141.0031005!2d43.1979638!2m3!6e0!7e2!8j1547146800!3e3) via 走走, ~10 min

小樽站到定山溪酒店
1. [小樽站到札幌站](https://www.google.com/maps/dir/Otaru+Station,+2+Chome-22+Inaho,+Otaru,+Hokkaido,+Japan/Sapporo+Station,+4+Chome+Kita+6+Jonishi,+Kita,+Sapporo,+Hokkaido,+Japan/@43.1323375,141.0325971,11z/data=!3m2!4b1!5s0x5f0b2974d2fff7a5:0x4ceb5b0533433580!4m18!4m17!1m5!1m1!1s0x5f0ae1acc9f79d0f:0x459dd44dd4dcb36b!2m2!1d140.9937088!2d43.1977462!1m5!1m1!1s0x5f0b2974d2c3f903:0xa5e2b18cdd4a47a5!2m2!1d141.3507553!2d43.0686606!2m3!6e0!7e2!8j1547146800!3e3) via JR, ~30 min
1. 札幌站到定山溪酒店 via 公交

> 是否可以叫定山溪酒店到札幌站接我们？

### 住

[定山溪酒店] aka. Jozankei Hotel

## 2/10

### 打卡

- [札幌国际滑雪场]

### 转移方程

坐私人公交从定山溪酒店去札幌国际滑雪场

坐私人公交从札幌国际滑雪场回定山溪酒店

### 住

[定山溪酒店] aka. Jozankei Hotel

## 2/11

### 打卡

- [网走破冰船](https://www.google.com/maps/place/Abashiri+Drift+Ice+Sightseeing+%26+Icebreaker+Ship/@44.020013,144.2709825,16.25z/data=!4m5!3m4!1s0x5f6d33a4e526c6a1:0x1cb5f3a10d22d4d4!8m2!3d44.0220136!4d144.2735848) @14:00
- [知床世界遗产中心](https://www.google.com/maps/place/Shiretoko+World+Heritage+Centre/@44.0697321,144.990771,17z/data=!3m1!5s0x5f752041ddddd87d:0x1426b2053c95b33b!4m26!1m20!4m19!1m6!1m2!1s0x5f6d48bec147e0cd:0xda3a6303793dd6f3!2zSmFwYW4sIOWls-a7oeWIq-acuuWcuiDvvIhIb2trYWlkb--8iQ!2m2!1d144.1584!2d43.88142!1m6!1m2!1s0x601d86f35dd04261:0xc7d03f9596f4d66e!2sKITAKOBUSHI+SHIRETOKO+Hotel+%26+Resort,+172+Utorohigashi,+Shari,+Hokkaido,+Japan!2m2!1d144.9949992!2d44.0720734!2m3!6e0!7e2!8j1549913400!3e3!3m4!1s0x5f6cc39275e13933:0x657d197d6ecd4e2b!8m2!3d44.0695265!4d144.9912125)
- [NPO协会](https://www.google.com/maps/place/NPO法人知床ナチュラリスト協会/@44.0718004,144.9948289,17z/data=!3m1!4b1!4m5!3m4!1s0x5f6cc38c35d41441:0xa0f9bcf0f1ed4624!8m2!3d44.0718004!4d144.9966739)

### 转移方程

[定山溪酒店]到[知床北辛夷酒店][kitakobushi shiretoko]
1. [定山溪酒店到札幌豪景酒店放行李](https://www.google.com/maps/dir/Japan,+%E5%8C%97%E6%B5%B7%E9%81%93%E6%9C%AD%E5%B9%8C%E5%B8%82%E5%8D%97%E5%8C%BA%E5%AE%9A%E5%B1%B1%E6%B8%93%E6%B8%A9%E6%B3%89%E8%A5%BF%EF%BC%94%E4%B8%81%E7%9B%AE%EF%BC%93%EF%BC%94%EF%BC%90+%E5%AE%9A%E5%B1%B1%E6%B8%93%E3%83%9B%E3%83%86%E3%83%AB/Sapporo+view+hotel,+8+Chome+Odorinishi,+Chuo+Ward,+Sapporo,+Hokkaido,+Japan/@43.0125379,141.1874053,12z/am=t/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x5f0ad21891910975:0x1a5249e226336383!2m2!1d141.161558!2d42.965078!1m5!1m1!1s0x5f0b299a599e75ed:0xc98051102e770376!2m2!1d141.3446963!2d43.0588028!3e3) via 公交 @6:00 速8, ~1 hr

    1. 7:05到[札幌南1西3]()
    1. 走10 min，7:15到札幌景豪酒店
    1. [札幌豪景酒店到札幌站](https://www.google.com/maps/dir/Sapporo+view+hotel,+8+Chome+Odorinishi,+Chuo+Ward,+Sapporo,+Hokkaido,+Japan/Sapporo+Station,+4+Chome+Kita+6+Jonishi,+Kita,+Sapporo,+Hokkaido,+Japan/@43.0637327,141.3448538,16z/data=!3m2!4b1!5s0x5f0b2974d2fff7a5:0x4ceb5b0533433580!4m14!4m13!1m5!1m1!1s0x5f0b299a599e75ed:0xc98051102e770376!2m2!1d141.3446963!2d43.0588028!1m5!1m1!1s0x5f0b2974d2c3f903:0xa5e2b18cdd4a47a5!2m2!1d141.3507553!2d43.0686606!3e3) via 走走, ~20 min

1. [札幌站到新千岁机场](https://www.google.com/maps/dir/Sapporo+Station,+4+Chome+Kita+6+Jonishi,+Kita,+Sapporo,+Hokkaido,+Japan/New+Chitose+Airport,+Bibi,+Chitose,+Hokkaido,+Japan/@42.9282477,141.3767185,11z/data=!3m2!4b1!5s0x5f752041ddddd87d:0x1426b2053c95b33b!4m18!4m17!1m5!1m1!1s0x5f0b2974d2c3f903:0xa5e2b18cdd4a47a5!2m2!1d141.3507553!2d43.0686606!1m5!1m1!1s0x5f7520459d5c1add:0x1aba607ce5f681f6!2m2!1d141.6704786!2d42.7925942!2m3!6e0!7e2!8j1549868400!3e3) via JR @8:05

    8:42到新千岁机场

1. 新千岁机场坐飞机去女满别机场
1. [女满别机场到网走站](https://www.google.com/maps/dir/Memambetsu+Airport,+Memanbetsuch%C5%AB%C5%8D,+%C5%8Czora-ch%C5%8D,+Abashiri-gun,+Hokkaid%C5%8D+099-2371,+Japan/Abashiri+Station,+2+Chome-2+Shinmachi,+Abashiri,+Hokkaido,+Japan/@43.9491143,144.130555,12z/data=!3m1!4b1!4m18!4m17!1m5!1m1!1s0x5f6d48bec147e0cd:0xda3a6303793dd6f3!2m2!1d144.1584!2d43.88142!1m5!1m1!1s0x5f6d339c27a03a43:0x6243903d1c1d420e!2m2!1d144.254375!2d44.019989!2m3!6e0!7e2!8j1548868440!3e3) via 公交 @10:35

破冰船到[知床北辛夷酒店][kitakobushi shiretoko]
1. via Shiretoko Airport Line公交 @15:13

<!-- 1. [札幌站到斜里站](https://www.google.com/maps/dir/Sapporo+Station,+4+Chome+Kita+6+Jonishi,+Kita,+Sapporo,+Hokkaido,+Japan/Shiretokoshari/@43.4123912,141.885805,8z/data=!3m2!4b1!5s0x5f0b2974d2fff7a5:0x4ceb5b0533433580!4m19!4m18!1m5!1m1!1s0x5f0b2974d2c3f903:0xa5e2b18cdd4a47a5!2m2!1d141.3507553!2d43.0686606!1m5!1m1!1s0x5f6d222fc326d82d:0x25669ee3b607ba06!2m2!1d144.661526!2d43.9109942!2m3!6e0!7e2!8j1549872000!3e3!5i3)

    - 北线 ~8 hr
    - 南线 ~7 hr -->

1. 斜里站到Kitakobushi Shiretoko酒店 via 公交

    Google Maps上搜不到这个公交，但是下火车之后有公交站（确信）。

> 是否可以叫酒店派车到斜里站接我们？

### 住

[Kitakobushi Shiretoko酒店 aka. 知床北辛夷酒店][kitakobushi shiretoko]

## 2/12

### 转移方程

[知床北辛夷][kitakobushi shiretoko]到札幌豪景酒店
1. 知床北辛夷酒店到斜里站 via 公交, ~50 min
1. [斜里站到网走站](https://www.google.com/maps/dir/Shiretokoshari/Abashiri+Station,+2+Chome-2+Shinmachi,+Abashiri,+Hokkaido,+Japan/@43.9520998,144.3699653,11.75z/data=!4m14!4m13!1m5!1m1!1s0x5f6d222fc326d82d:0x25669ee3b607ba06!2m2!1d144.661526!2d43.9109942!1m5!1m1!1s0x5f6d339c27a03a43:0x6243903d1c1d420e!2m2!1d144.254375!2d44.019989!3e3)
1. [网走站到网走监狱博物馆](https://www.google.com/maps/dir/Abashiri+Station,+2+Chome-2+Shinmachi,+Abashiri,+Hokkaido,+Japan/Abashiri+Prison+Museum,+1+Yobito,+Abashiri,+Hokkaido,+Japan/@44.0084824,144.2240321,14z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x5f6d339c27a03a43:0x6243903d1c1d420e!2m2!1d144.254375!2d44.019989!1m5!1m1!1s0x5f6d346b1cc3b181:0xa7d6c8cb14d85eed!2m2!1d144.22933!2d43.9956975!3e3) via 公交, ~30 min
1. 网走监狱博物馆到女满别站 @19:00 天都山入口 via 大巴, ~30 min

1. [札幌站到札幌豪景酒店](https://www.google.com/maps/dir/Sapporo+Station,+3+Chome-4+Chome+Kita+6+Jonishi,+Kita,+Sapporo,+Hokkaido,+Japan/Sapporo+view+hotel,+8+Chome+Odorinishi,+Chuo+Ward,+Sapporo,+Hokkaido,+Japan/@43.0637327,141.3432314,16z/data=!3m2!4b1!5s0x5f0b2974d2fff7a5:0x4ceb5b0533433580!4m18!4m17!1m5!1m1!1s0x5f0b2974d2c3f903:0xa5e2b18cdd4a47a5!2m2!1d141.3507553!2d43.0686606!1m5!1m1!1s0x5f0b299a599e75ed:0xc98051102e770376!2m2!1d141.3446963!2d43.0588028!2m3!6e0!7e2!8j1547146800!3e2) via 走走, ~20 min

> 是否可以叫札幌豪景酒店派车到札幌站接我们？
>
> 好像也不用……走走也能接受。

### 住

[札幌豪景酒店][札幌豪景酒店] aka. Sapporo View Hotel

## 2/13

### 转移方程

札幌豪景酒店到新千岁机场
1. 札幌豪景酒店到札幌站
1. 札幌站到新千岁机场

新千岁机场到上海浦东机场
1. 新千岁机场到东京羽田机场
1. 东京羽田机场到上海浦东机场

[定山溪酒店]: https://www.google.com/maps/place/%E5%AE%9A%E5%B1%B1%E6%B8%93%E3%83%9B%E3%83%86%E3%83%AB/@42.9666415,141.1626453,15.25z/data=!4m5!3m4!1s0x0:0x1a5249e226336383!8m2!3d42.9650789!4d141.1615571
[知床诺贝尔酒店]: https://www.google.com/maps/place/%E7%9F%A5%E5%BA%8A%E3%83%8E%E3%83%BC%E3%83%96%E3%83%AB%E3%83%9B%E3%83%86%E3%83%AB/@44.0706205,144.9930125,15.75z/data=!4m8!1m2!2m1!1sshiretoko+noble+hotel!3m4!1s0x0:0x3863683c689ce032!8m2!3d44.0704284!4d144.9933422
[札幌豪景酒店]: https://www.google.com/maps/place/Sapporo+view+hotel/@43.0577752,141.3427808,16z/data=!4m5!3m4!1s0x0:0xc98051102e770376!8m2!3d43.0588027!4d141.3446957
[kitakobushi shiretoko]: https://www.google.com/maps/place/KITAKOBUSHI+SHIRETOKO+Hotel+%26+Resort/@44.0720734,144.9928052,17z/data=!3m1!4b1!4m5!3m4!1s0x601d86f35dd04261:0xc7d03f9596f4d66e!8m2!3d44.0720734!4d144.9949992
[札幌Classe酒店]: https://www.google.com/maps/place/Sapporo+view+hotel/@43.0596527,141.3083651,12.75z/data=!4m5!3m4!1s0x5f0b299a599e75ed:0xc98051102e770376!8m2!3d43.0588028!4d141.3446964

[永专寺]: https://www.google.com/maps/place/%E6%B0%B8%E5%B0%82%E5%AF%BA/@44.0197975,144.2696753,19.25z/data=!4m5!3m4!1s0x5f6d33a3300527d7:0xc4ff243625465420!8m2!3d44.0195172!4d144.2701874
[札幌国际滑雪场]: https://www.google.com/maps/place/Sapporo+Kokusai+Ski+Resort/@43.0728166,141.0820066,17z/data=!3m1!4b1!4m5!3m4!1s0x5f0adbce3e252739:0xeab3b0f2ca4ec210!8m2!3d43.0728166!4d141.0842006

> 是否……算了